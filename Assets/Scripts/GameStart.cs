﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour {

    bool startGame = false;
    public const string LAYER_NAME = "Danger";
    public int sortingOrder = 0;
    private SpriteRenderer sprite;

    // Use this for initialization
    void Start () {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sortingOrder = 11;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKey && sprite.sortingOrder != -1)
        {
            sprite.sortingOrder = -1;
            startGame = true;
        }
	}
}
