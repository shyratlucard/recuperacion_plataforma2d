﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightPlayer : MonoBehaviour {

    Rigidbody2D knightRB;
    public float maxSpeed;
    Animator knightAnim;
    //Collision2D knightColl;

    bool canMove = false;

    //Jump
    bool onFloor = false;
    float checkFloorRadius = 0.2f;
    public LayerMask floorLayer;
    public Transform checkFloor;
    public float jumpPower;    

    //Danger
    //bool hitDanger = false;
    //float checkDangerRadius = 0.2f;
    //public LayerMask dangerLayer;
    //public Transform checkDanger;

    bool turnKnight = true;
    SpriteRenderer knightRenderer;

	// Use this for initialization
	void Start () {
        knightRB = GetComponent<Rigidbody2D>();
        knightRenderer = GetComponent<SpriteRenderer>();
        knightAnim = GetComponent<Animator>();
        //knightColl = GetComponent<Collision2D>();
    }
	
	// Update is called once per frame
	void Update () {
        //hitDanger = GetComponent<Collision>();
        //if (canMove && hitDanger)
        //{
        //    GameOver.Instance.ShowGameOver();
        //}

        //OnCollisionEnter2D(knightColl);

        if (canMove && onFloor && Input.GetAxis("Jump") > 0)
        {
            knightAnim.SetBool("onFloor", false);
            knightRB.velocity = new Vector2(knightRB.velocity.x, 0f);
            knightRB.AddForce(new Vector2(0, jumpPower), ForceMode2D.Impulse);
            onFloor = false;
        }

        onFloor = Physics2D.OverlapCircle(checkFloor.position, checkFloorRadius, floorLayer);
        knightAnim.SetBool("onFloor", onFloor);


        float move = Input.GetAxis("Horizontal");
        if (canMove)
        {
            if (move > 0 && !turnKnight)
            {
                Turn();
            }
            else if (move < 0 && turnKnight)
            {
                Turn();
            }

            knightRB.velocity = new Vector2(move * maxSpeed, knightRB.velocity.y);

            knightAnim.SetFloat("speedMov", Mathf.Abs(move));
        }
        else
        {
            knightRB.velocity = new Vector2(0, knightRB.velocity.y);

            knightAnim.SetFloat("speedMov", 0);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            GameOver.Instance.ShowGameOver();
        }

        if (collision.gameObject.tag.Equals("Victory"))
        {
            GameVictory.Instance.ShowVictory();
        }
    }

    void Turn()
    {
        turnKnight = !turnKnight;
        knightRenderer.flipX = !knightRenderer.flipX;
    }

    public void ToggleCanMove()
    {
        canMove = !canMove;
    }


    //bool isGrounded = false;

    //void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.layer == 8 //check the int value in layer manager(User Defined starts at 8) 
    //        && !isGrounded)
    //    {
    //        isGrounded = true;
    //    }
    //}
    //void OnCollisionExit(Collision collision)
    //{
    //    if (collision.gameObject.layer == 8
    //        && isGrounded)
    //    {
    //        isGrounded = false;
    //    }
    //}

        //knightRB.velocity = new Vector2(knightRB.velocity.x, 0f);
    }
