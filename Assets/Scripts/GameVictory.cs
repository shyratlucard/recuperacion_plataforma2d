﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameVictory : MonoBehaviour {

    public const string LAYER_NAME_VICTORY = "VictoryScreen";
    public int sortingOrderVictory = 0;
    private SpriteRenderer sprite;

    public static GameVictory Instance;

    // Use this for initialization
    void Start () {
        Instance = (GameVictory)GameObject.FindObjectOfType(typeof(GameVictory));

        sprite = GetComponent<SpriteRenderer>();
    }

    public void ShowVictory()
    {
        sprite.sortingOrder = 21;
    }

    // Update is called once per frame
    //void Update () {

    //}
}
