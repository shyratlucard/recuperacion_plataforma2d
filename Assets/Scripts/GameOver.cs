﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {


    public const string LAYER_NAME = "Danger";
    public int sortingOrder = 0;
    private SpriteRenderer sprite;

    public static GameOver Instance;

    void Start()
    {
        Instance = (GameOver)GameObject.FindObjectOfType(typeof(GameOver));

        sprite = GetComponent<SpriteRenderer>();
    }

    public void ShowGameOver()
    {
        sprite.sortingOrder = 20;
    }

    // Update is called once per frame
 //   void Update ()
 //   {
		
	//}
}
